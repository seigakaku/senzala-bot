{ pkgs ? import <nixpkgs> { } }:
let
  pyPkgs = p: with p; [
    pyyaml
    pytest
    faker
    coloredlogs
    watchdog
    pytimeparse
    #########################
    (buildPythonPackage rec {
      pname = "discord.py";
      version = "2.2.2";
      src = fetchPypi {
        inherit pname version;
        sha256 = "sha256-uZRAVry1cRstBAiISP0ARGbPEXwVyE+nmL9VRw8oJ18=";
      };
      doCheck = false;
      propagatedBuildInputs = with pkgs.python310Packages; [ aiohttp ];
    })
  ];
in
pkgs.mkShell {
  packages = with pkgs; [
    python3Packages.flake8
    pylint
    nixpkgs-fmt
    sqlite
    (python3.withPackages pyPkgs)
  ];
}
