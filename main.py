#!/usr/bin/env python
# -*- mode: Python -*-
"""Senzala bot"""

import os
import sys
import coloredlogs
import logging
import sqlite3
from argparse import ArgumentParser
from io import StringIO
from dataclasses import dataclass
from datetime import datetime, timedelta
from typing import TYPE_CHECKING, Optional
from contextlib import contextmanager
from collections import UserDict
from pathlib import Path

import yaml
import discord

from discord import AuditLogEntry, Member, Object, Permissions, Role, Status, Intents
from discord import AuditLogAction as AAction
from discord.ext.commands import Bot

from pytimeparse.timeparse import timeparse

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

if TYPE_CHECKING:
    from typing import Union, Tuple, Dict, Any
    from discord import Guild, User
    from sqlite3 import Connection

# =========================================
# == Config

CONFIG_PATH = Path("./config.yaml")
MODULE_NAME = 'bot'

log = logging.getLogger(MODULE_NAME)
config = None
quotas = None
db = None

intents: 'Intents' = Intents(
    bans=True,
    guilds=True,
    members=True,
    messages=True,
    message_content=True,
    moderation=True,
)
bot: 'Bot' = Bot(command_prefix='!', intents=intents)

role_control_whitelist: 'List[int]' = []
mod_control_whitelist: 'List[int]' = []
default_controlled_permissions: 'List[str]' = []

# ========================================
# == Utils

def eprint(*args, **kwargs):
    """Print to STDERR"""
    print(*args, file=sys.stderr, **kwargs)

def role_fmt(role: 'Optional[Role]') -> 'str':
    """Return a string representation of a discord ROLE"""
    if not role:
        return 'Invalid role'
    return f'"{role.name}" ({role.id})'

def user_fmt(user: 'Optional[Union[User, Member, AuditLogEntry]]') -> 'str':
    """Return a string representation of a discord USER"""
    if not user or isinstance(user, Object):
        return 'Unknown'
    name = f'{user.name} ' if user.name else ''
    return f'{name}({user.id})'

def al_entry_user_fmt(entry: 'AuditLogEntry') -> 'str':
    """Return a string representation of the source and target users of an audit log ENTRY"""
    return f'by {user_fmt(entry.user)} on {user_fmt(entry.target)}'

def get_specialized_config(prefix: 'str', specifier: 'Optional[str]' = None) -> 'Any':
    """
    Fetch either a specialized value denoted by SPECIFIER of the configuration key denoted by PREFIX
    from the configuration file, otherwise, if SPECIFIER is None, or that value was not found,
    return the default, which is the value of the key denoted by PREFIX.
    """
    default: 'Any' = config[prefix] # must exist
    if not specifier:
        return default
    try:
        spec: 'Any' = config[f'{prefix}_{specifier}']
        log.debug("Using specialized config %s_%s = %s", prefix, str(specifier), str(spec))
        return spec
    except KeyError:
        return default

def controlled_permissions(**kwargs) -> 'Permissions':
    """
    Generate a Permissions object from the KWARGS,
    If KWARGS contains the 'snowflake' key, try to get specialized permissions for the discord user
    with the id = 'snowflake'.
    If KWARGS contains any more keyword arguments, these name fields to be set in the newly created
    Permissions object.
    """
    perms: 'Permissions' = Permissions.none()

    ctrl: 'List[str]' = default_controlled_permissions
    if "snowflake" in kwargs:
        if kwargs["snowflake"] is not None: # If ID is in kwargs, lookup specialized permissions
            ctrl = get_specialized_config('role_controlled_permissions', str(kwargs["snowflake"]))
        kwargs.pop("snowflake")

    for perm in ctrl:
        setattr(perms, perm, True)

    for perm, value in kwargs.items():
        setattr(perms, perm, value)

    return perms

def role_has_controlled_permissions(role: 'Role', **kwargs) -> 'bool':
    """
    Check if ROLE contains controlled permissions, these permissions are described in
    the function CONTROLLED_PERMISSIONS, to which KWARGS is passed.
    """
    return (role.permissions & controlled_permissions(**kwargs)) != Permissions.none()

# =========================================


@bot.event
async def on_ready():
    """Runs when bot logs in."""

    self_id: 'int' = bot.user.id
    log.info("Bot Logged In. ID: %s", self_id)

    log.info("IDs excluded from role control: %s", role_control_whitelist)
    log.info("IDs excluded from moderation control: %s", mod_control_whitelist)

    # Appending bot's own id to whitelist whitelist
    role_control_whitelist.append(self_id)
    mod_control_whitelist.append(self_id)

    status: 'str' = getattr(Status, config['status'])
    log.info("Changing bot status to %s", status)
    await bot.change_presence(status=status)

# ============================================
# == Commands

@bot.command(name="!untimeout")
async def untimeout(ctx: 'commands.Context', member: 'Optional[Member]'):
    """
    Bot command to remove the timeout of MEMBER,
    or every timed out member in a guild if MEMBER is None
    """
    if member:
        log.info("Removing timeout from %s", user_fmt(member))
        await member.timeout(None, reason='Untimeout command requested.')
    else:
        for member_ in [ m for m in ctx.guild.members if m.is_timed_out() ]:
            log.info("Removing timeout from %s", user_fmt(member_))
            await member_.timeout(None, reason='Global timeout removal.')

# ===========================================
# == Role control

class RoleControlDB:

    parser = ArgumentParser(prog='rolewl', add_help=True, allow_abbrev=True, exit_on_error=False)
    parser.add_argument('-c', '--clear', metavar='ID', nargs='*', help='Clear all entries for users')
    parser.add_argument('-l', '--list', action='store_true', help='List role control entries for users')

    parser.add_argument('-a', '--add', metavar='ID', nargs='+', help='Add or update entry for users')
    parser.add_argument('-r', '--roles', metavar='ROLES', nargs='+', help='Apply only to those roles')
    parser.add_argument('-u', '--until', metavar="TIME", type=str, help='Valid for how long')
    parser.add_argument('-p', '--purge', action='store_true', help='Clean expired entries')

    def insert(ids: 'List[int]', until: 'Optional[int]', roles: 'List[int]'):
        has_roles = len(roles) > 0
        data = [ (snowflake, until, not has_roles) for snowflake in ids ]
        with db_cursor() as cursor:
            log.debug('Whitelisting %s on roles %s until %s', ids, roles, until)
            cursor.executemany("INSERT OR REPLACE INTO RoleControl (id, until, free) VALUES (?, ?, ?)", data)
            if has_roles:
                product = [ (snowflake, role) for snowflake in ids for role in roles  ]
                cursor.executemany("INSERT OR REPLACE INTO RoleControlRoles (id, role_id) VALUES (?, ?)", product)

    def purge():
        with db_cursor() as cursor:
            log.debug("Purging database of expired entries.")
            cursor.execute("DELETE FROM RoleControl WHERE (until IS NOT NULL AND :now > until)",
                           {"now": datetime.now().timestamp()})

    def clear(ids: 'Optional[List[int]]'):
        allp = not ids or len(ids) <= 0
        with db_cursor() as cursor:
            if allp:
                log.debug("Clearing whitelist database")
                cursor.execute("DELETE FROM RoleControl");
            else:
                log.debug("Clearing whitelist for ids: %s", ids)
                query = f"DELETE FROM RoleControl WHERE id IN ({','.join('?' * len(ids))})"
                cursor.execute(query, tuple(ids))

    def check(snowflake: 'int', role_id: 'int') -> 'bool':
        with db_cursor() as cursor:
            cursor.execute("""
            SELECT EXISTS (
                SELECT * FROM RoleControl as r
                LEFT JOIN RoleControlRoles as rr
                    ON (r.id = rr.id)
                WHERE
                r.id = :id AND
                (r.free = TRUE OR rr.role_id = :role_id) AND
                (r.until IS NULL OR :now <= r.until)
            );
            """, {"id": snowflake, "role_id": role_id, "now": datetime.now().timestamp()})
            res = cursor.fetchone()
            return res and res[0] == 1

    @bot.command()
    async def rolewl(ctx, *args):
        content = ctx.message.content.split(' ')[1:]
        args = RoleControlDB.parser.parse_args(content)

        if args.add and (args.clear is not None):
            log.warn('Invalid role whitelist command: request has both "add" and "clear"')
            await ctx.send("Can't add and clear at the same time (-a && -c)")
            return

        # Validate IDs
        for snowflake in (args.add or []):
            try:
                as_int = int(snowflake)
                await bot.fetch_user(as_int)
            except ValueError:
                log.warn('Invalid role whitelist command: Invalid ID: %s', snowflake)
                await ctx.send(f"{snowflake}: Invalid user id, not an integer.")
            except NotFound:
                log.warn('Invalid role whitelist command: Invalid discord user ID: %s', snowflake)
                await ctx.send(f"{snowflake}: Not a valid discord user.")
            except HttpError:
                log.error('Failed to fetch discord user with id: %s', snowflake)
                await ctx.send(f"{snowflake}: Couldn't valid user id.")

        ###############

        if args.list:
            pass

        if args.purge:
            RoleControlDB.purge()

        if args.clear is not None:
            RoleControlDB.clear(args.clear)

        ################

        if args.add:
            until = None
            if args.until:
                sec = timeparse(args.until)
                if sec:
                    delta = timedelta(seconds=sec)
                    until = (datetime.now() + delta).timestamp()

            RoleControlDB.insert(args.add, until, args.roles or [])

def role_controlled_p(role: 'Union[Role, Object]', **kwargs) -> 'bool':
    """Check if role should be controlled."""
    return isinstance(role, Role) and \
        not role.name == "@everyone" and \
        role_has_controlled_permissions(role, **kwargs)

async def negate_roles(member: 'Member', roles: 'List[Role]'):
    """Negate the addition of ROLES to the a MEMBER"""
    if not roles:
        return
    if log.isEnabledFor(logging.INFO):
      for role in roles:
          log.info('Removing role %s from %s', role_fmt(role), user_fmt(member))
    await member.remove_roles(*roles, reason='Cancelling role update with controlled roles')

async def delete_roles(roles: 'List[Role]'):
    """Delete ROLES"""
    for role in roles:
        log.info("Deleting role %s", role_fmt(role))
        await role.delete(reason='Role with controlled permissions assigned by unauthorized user.')

async def revert_roles(roles: 'List[Role]'):
    """Revert enabled controlled permissions in ROLES to a disabled state."""
    for role in roles:
        new_perms: 'Permissions' = role.permissions ^ controlled_permissions()
        log.info("Sanitized role %s", role_fmt(role))
        log.debug("Editing role %s - Perms: %s -> %s",
                      role_fmt(role), role.permissions, new_perms)
        await role.edit(permissions=new_perms)

async def demote_members(members: 'List[Member]'):
    """Remove all roles privileged controlled permissions from MEMBERS"""
    for member in members:

        user_privileged_roles: 'List[Role]' = []

        for role in member.roles:
            if role_has_controlled_permissions(role, snowflake=member.id, manage_roles=True):
                user_privileged_roles.append(role)

        if user_privileged_roles:
            if log.isEnabledFor(logging.INFO):
              for role in user_privileged_roles:
                  log.info("Removing role %s from %s", role_fmt(role), user_fmt(member))
            reason = 'Removing controlled roles from member due to unauthorized action.'
            await member.remove_roles(*user_privileged_roles, reason=reason)
        else:
            log.warning("Member %s scheduled for demotion, but doesn't have controlled roles.",
                            user_fmt(member))

async def kick_bots(bots: 'List[Member]'):
    for bot in bots:
        log.info("Bot with unauthorized roles added, KICKING: %s", user_fmt(bot))
        await bot.kick(reason="Bot has unauthorized roles")

async def handle_role_control(entry: 'AuditLogEntry'):
    """Process audit log ENTRY for unauthorized actions on roles"""
    # entry is member_role_update, role_create or role_update

    # Check master whitelist
    if entry.user_id in role_control_whitelist:
        log.debug("Role action by user in master whitelist: %s", user_fmt(entry.user))
        return

    roles = entry.target.roles if entry.action == AAction.bot_add else entry.after.roles
    if all([RoleControlDB.check(entry.user_id, role.id) for role in roles]):
        log.debug("WhitelistDB role action: %s on roles: %s", user_fmt(entry.user), entry.after.roles)
        return

    roles_to_negate: 'List[Role]' = []
    roles_to_revert: 'List[Role]' = []
    roles_to_delete: 'List[Role]' = []
    members_to_demote: 'List[Member]' = []
    bots_to_kick: 'List[Member]' = []

    if entry.action in [ AAction.member_role_update, AAction.bot_add ]:
        # Check if it's role adding or role removal, don't act on role removal
        if entry.action == AAction.member_role_update:
            if len(entry.after.roles) < len(entry.before.roles):
                log.debug("Role removed %s: %s -> %s",
                            al_entry_user_fmt(entry), entry.before.roles, entry.after.roles)
                return

            log.debug("Role added %s: %s -> %s",
                        al_entry_user_fmt(entry), entry.before.roles, entry.after.roles)

        def unauthorizedp(role: 'Role') -> 'bool':
            return role_controlled_p(role, snowflake=entry.user_id)

        roles = entry.after.roles if entry.action == AAction.member_role_update else entry.target.roles
        unauthorized_roles: 'List[Role]' = [role for role in roles if unauthorizedp(role)]

        for role in roles:
            if role in unauthorized_roles:
                roles_to_negate.append(role)
            log.info("Role assignment action of %s passed restriction checks", role_fmt(role))
        if unauthorized_roles:
            members_to_demote.append(entry.user)
            if entry.action == AAction.bot_add:
                bots_to_kick.append(entry.target)
    else: # role create/update
        role: 'Role' = entry.target

        # Role doesn't exist anymore?
        if not isinstance(role, Role):
            log.debug("Role %s returned as object?", role.id )
            return

        if role_has_controlled_permissions(role, snowflake=entry.user_id):
            #TODO: Improve distinction between 'created' and 'updated'
            # Updated roles go back to original state without controlled perms
            if entry.action == AAction.role_update:
                roles_to_revert.append(entry.target)
            else: # New roles are deleted
                roles_to_delete.append(role)
            members_to_demote.append(entry.user)
            return
        log.debug("Role update/create of %s passed restriction checks", role_fmt(role))

    # Remove bots that were added with unauthorized roles
    await kick_bots(bots_to_kick)
    # Remove roles from users that were added but have controlled permissions
    await negate_roles(entry.target, roles_to_negate)
    # Delete new roles created with controlled permissions
    await delete_roles(roles_to_delete)
    # Revert updated roles with newly added controlled permissions
    await revert_roles(roles_to_revert)
    # Remove controlled roles from users who've performed an unauthorized action
    await demote_members(members_to_demote)

# ===========================================
# == Moderation control

def get_hitquota(snowflake: 'Optional[int]') -> 'int':
    """Helper method to fetch user's moderation hitquota from config"""
    return get_specialized_config('moderation_hitquota', str(snowflake))

def get_timequota(snowflake: 'Optional[int]') -> 'timedelta':
    """Helper method to fetch user's moderation timequota from config"""
    timespec: 'str' = get_specialized_config('moderation_timequota', str(snowflake))
    #TODO: Add custom timeformat spec from config file
    _tq: 'datetime' = datetime.strptime(timespec, "%H:%M:%S")
    return timedelta(hours=_tq.hour, minutes=_tq.minute, seconds=_tq.second)

async def perform_quota_exceeded_action(member: 'Member'):
    """Enact some action upon MEMBER when it's quota is reached"""
    if not member:
        log.error("Invalid member when applyting quota exceeded action: %s", member)
        return
    reason = 'Moderation action quota reached.'
    action: 'str' = get_specialized_config('moderation_quota_action', str(member.id))
    match action.lower():
        case 'kick':
            log.info("Moderation quota reached, KICKING: %s", user_fmt(member))
            await member.kick(reason=reason)
        case 'ban':
            log.info("Moderation quota reached, BANNING: %s", user_fmt(member))
            await member.ban(reason=reason)

@contextmanager
def db_cursor():
    cursor = db.cursor()
    try:
        yield cursor
        db.commit()
    finally:
        cursor.close()

@dataclass
class QuotaRecord:
    """Helper class for manipulating user moderation action quotas"""

    hits: 'int'
    start: 'datetime'
    db: 'sqlite3.Connection' = db

    def __init__(self,
                 hits: 'int',
                 start: 'Union[datetime, int, float]',
                 db: 'Optional[sqlite3.Connection]' = None):
        if isinstance(start, datetime):
            self.start = start
        elif isinstance(start, int) or isinstance(start, float):
            self.start = datetime.fromtimestamp(start)
        else:
            logging.error("Invalid start type for quota %s", type(start))

        if db:
            self.db = db

        self.hits = hits


    @staticmethod
    def new(snowflake: 'int',
            start: 'Optional[datetime]' = None) -> 'QuotaRecord':
        """Create a new QuotaRecord with initial values"""
        start_ = start if start else datetime.now()
        hits = 1

        cursor = db.cursor()
        cursor.execute("INSERT INTO Quotas (id, hits, start) VALUES (?, ?, ?)",
                        (snowflake, hits, start_.timestamp()))
        cursor.close()
        db.commit()

        return QuotaRecord(hits, start_)

    def delete(self, snowflake: 'int'):
        if quotas and snowflake in quotas:
            quotas.pop(snowflake)
        with db_cursor() as cursor:
            cursor.execute("DELETE FROM Quotas WHERE id = ?", (snowflake, ))

    def hit(self, snowflake: 'int') -> 'int':
        """Update a QuotaRecord by incrementing it's hitcount"""
        self.hits += 1

        with db_cursor() as cursor:
            cursor.execute("UPDATE Quotas SET hits = ? WHERE id = ?", (self.hits, snowflake))

        return self.hits

    def expiredp(self, timequota: 'timedelta', when: 'Optional[datetime]' = None) -> 'bool':
        """Check if a QuotaRecord period TIMEQUOTA is expired at present."""
        return (when if when else datetime.now()) >= self.start + timequota

    def elapsed(self, when: 'Optional[datetime]' = None) -> 'timedelta':
        """Get how much time has passed since the start of this QuotaRecord time block."""
        return (when if when else datetime.now()) - self.start

    def to_string(self, timequota: 'timedelta', hitquota: 'int') -> 'str':
        """Return a detailed string representation of the quota."""
        tzero: 'datetime' = self.start
        tone: 'datetime' = tzero + timequota
        tminus1: 'timedelta' = timequota - self.elapsed()
        tdelta: 'timedelta' = self.elapsed()
        return f"T0 {tzero}; T1 {tone}; T-1 {tminus1}: TΔ {tdelta}; Hits {self.hits}/{hitquota}"

    @staticmethod
    def clean_expired_quotas(quotas: 'Dict[int, QuotaRecord]',
                             timequota_override: 'Optional[timedelta]' = None,
                             when: 'Optional[datetime]'=None) -> 'Dict[int, QuotaRecord]':
        to_delete: 'Dict[int, QuotaRecord]' = {}
        valid: 'Dict[int, QuotaRecord]' = {}
        for snowflake, quota in quotas.items():
            timequota = (timequota_override or get_timequota(snowflake))
            if quota.expiredp(timequota, when=when):
                log.debug("Deleting expired quota from database for: %s", snowflake)
                to_delete[snowflake] = quota
            else:
                log.debug("Keeping stil valid quota for %s until %s", snowflake, quota.start + timequota)
                valid[snowflake] = quota

        for snowflake, quota in to_delete.items():
            quota.delete(snowflake)

        return valid

    @staticmethod
    def load_quotas(clean_expired: 'bool' = True) -> 'Dict[int, QuotaRecord]':

        cursor = db.cursor()
        res = cursor.execute("SELECT * FROM Quotas;")

        quotas = { snowflake: QuotaRecord(hits, timestamp) for snowflake, hits, timestamp in res.fetchall() }
        if clean_expired:
            quotas = QuotaRecord.clean_expired_quotas(quotas)
        cursor.close()

        return quotas

async def handle_moderation_control(entry: 'AuditLogEntry'):
    """Process an audit log ENTRY for unauthorized moderation actions"""
    snowflake = entry.user_id
    if snowflake in mod_control_whitelist:
        log.info("Moderation action by whitelisted user: %s", user_fmt(entry.user))
        return

    timequota: 'timedelta' = get_timequota(snowflake)

    # New or time elapsed, set start time to now
    if snowflake not in quotas or quotas[snowflake].expiredp(timequota):
        quotas[snowflake] = QuotaRecord.new(snowflake)
    else:
        quotas[snowflake].hit(snowflake)

    quota: 'QuotaRecord' = quotas[snowflake]
    hitquota: 'int' = get_hitquota(snowflake)
    log.info("Moderation action %s - %s", entry.action, al_entry_user_fmt(entry))
    log.debug("Quota for %s: %s", user_fmt(entry.user), quota.to_string(timequota, hitquota))

    if quota.hits >= hitquota:
        quota.delete(snowflake)
        await perform_quota_exceeded_action(entry.user)

@bot.event
async def on_audit_log_entry_create(entry: 'AuditLogEntry'):
    """Triggered when an audit log entry is created."""
    match entry.action:
        case AAction.member_role_update | AAction.role_create | AAction.role_update | AAction.bot_add:
            await handle_role_control(entry)
        case AAction.kick | AAction.ban:
            await handle_moderation_control(entry)

# =========================================


# =========================================
# == Setup

def setup_database(db: 'sqlite3.Connection'):

    db.set_trace_callback(log.debug)

    cursor = db.cursor()
    cursor.execute("PRAGMA FOREIGN_KEYS = ON") # Pragma must be outside transction...
    cursor.execute("BEGIN")
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS quotas (
        id UNSIGNED BIG INT PRIMARY KEY ASC UNIQUE NOT NULL,
        hits SMALLINT NOT NULL DEFAULT (1) CHECK (hits > 0),
        start DATETIME NOT NULL DEFAULT (unixepoch())
    );
    """)
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS RoleControl (
        id UNSIGNED BIG INT PRIMARY KEY ASC UNIQUE NOT NULL,
        until DATETIME NULL DEFAULT (NULL),
        free TINYINT NOT NULL DEFAULT (TRUE)
    );
    """)
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS RoleControlRoles (
        id UNSIGNED BIG INT NOT NULL,
        role_id UNSIGNED BIG INT NOT NULL,

        CONSTRAINT fk_id FOREIGN KEY (id) REFERENCES RoleControl (id) ON DELETE CASCADE,
        CONSTRAINT unique_row UNIQUE (id, role_id)
    );
    """)
    cursor.execute("COMMIT")
    db.commit()
    cursor.close()
    return db

##############################################################

class ConfigMutationObserver(FileSystemEventHandler):
    def on_modified(self, event: 'FileModifiedEvent'):
        log.debug("Modification in config file detected.")
        config.repopulate_from_file()

class Config(UserDict):

    CONFIG_PATH: 'str' = './config.yaml'
    _yaml: 'Dict[str, Any]' = {}
    _observer: 'Observer'

    def __setitem__(self, key, value):
        self._yaml[key] = value

    def __getitem__(self, key):
        return self._yaml[key]

    def repopulate_from_file(self):
        log.debug("Repopulating configuration structure from file at %s", self.CONFIG_PATH)
        with open(self.CONFIG_PATH, 'r', encoding='utf-8') as conffile:
            self._yaml = yaml.load(conffile, Loader=yaml.FullLoader)

    def start_change_observer(self) -> 'Observer':
        handler = ConfigMutationObserver()
        self._observer = Observer()
        self._observer.schedule(handler, self.CONFIG_PATH)
        self._observer.start()
        return self._observer

    def __init__(self, path: 'Optional[str]' = None):
        if path:
            self.CONFIG_PATH = path
        self.repopulate_from_file()

def get_token() -> 'str':
    try:
        return os.environ['BOT_TOKEN']
    except KeyError:
        try:
            return config['bot_token']
        except KeyError:
            log.critical('Please set BOT_TOKEN evvar, or set it in config.yaml')
            sys.exit(1)

def setup_logging():
    log_level_ = get_specialized_config('log_level', 'main')
    log_level = getattr(logging, log_level_.upper())
    logging.basicConfig(level=log_level) # Root
    coloredlogs.install(level=log_level)
    coloredlogs.install(logger=log, level=log_level)

    formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s')

    try:
        logfile = config['log_file']
        if len(logfile) > 0:
            handler = logging.FileHandler(filename=logfile, encoding='utf-8', mode='w')
            handler.setFormatter(formatter)
            log.addHandler(handler)
    except KeyError:
        pass

    try:
        use_syslog = config['use_syslog']
        if use_syslog:
            try:
                syslog_address = config['syslog_address']
            except KeyError:
                syslog_address = '/dev/log'
            handler = logging.SyslogHandler(address=syslog_address)
            handler.setFormatter(formatter)
            log.addHandler(handler)
    except KeyError:
        pass

    discord_log_level_ = get_specialized_config('log_level', 'discord')
    discord_log_level = getattr(logging, discord_log_level_.upper())
    logging.getLogger("discord").setLevel(discord_log_level)


if __name__ == '__main__':
    config = Config()
    config_observer = config.start_change_observer()

    role_control_whitelist = config['role_control_whitelist']
    mod_control_whitelist = config['mod_control_whitelist']
    default_controlled_permissions: 'List[str]' = config['role_controlled_permissions']

    setup_logging()

    db = setup_database(sqlite3.connect(config['database_file']))
    quotas = QuotaRecord.load_quotas(clean_expired=True)

    token = get_token()
    bot.run(token, reconnect=True, log_handler=None) # We handle logging ourselves
