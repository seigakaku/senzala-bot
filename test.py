from argparse import ArgumentParser
import sys

parser = ArgumentParser(add_help=True, allow_abbrev=True, exit_on_error=False)
parser.add_argument('-n', '--new', metavar="ID", nargs='+', help='Add a new entry for users')
parser.add_argument('-e', '--edit', metavar="ID", nargs='+', help='Edit entry for users')
parser.add_argument('-r', '--roles', metavar='ROLES', nargs='?', help='Apply only to those roles')
parser.add_argument('-l', '--list', action='store_true', help='List role control entries for users')
parser.add_argument('-c', '--clear', action='store_true', help='Clear all entries for users')

print(parser.parse_args())
